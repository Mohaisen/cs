# from sympy import *
# x, y = symbols('x,y')


from sympy.abc import x, y, z
from sympy.abc import A, B, C, D

print(y | (x & y))
# y | (x & y)
print(x | y)
# x | y
print(~x)
# ~x


print((y & x).subs({x: True, y: True}))
# True
print((x | y).atoms())
# {x, y}


from sympy.logic import simplify_logic

b = (~x & ~y & ~z) | ( ~x & ~y & z)
print(b)
print(simplify_logic(b))
# ~x & ~y


from sympy.logic.boolalg import to_cnf

print(to_cnf(~(A | B) | D))
# (D | ~A) & (D | ~B)
print(to_cnf((A | B) & (A | ~A), True))
# A | B


from sympy.logic.boolalg import to_dnf

print(to_dnf(B & (A | C)))
# (A & B) | (B & C)
to_dnf((A & B) | (A & ~B) | (B & C) | (~B & C), True)
# A | C



from cs.boolean import create_truth_table

create_truth_table((~x & ~y & ~z) | ( ~x & ~y & z))

create_truth_table(~x & ~y)
create_truth_table(~x & ~y | x & z | ~z & y)


