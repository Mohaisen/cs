
from cs.turing import *

m = Machine({
    (0, S):(1, S, R),
    (1, '0'):(1, '1', R),
    (1, '1'):(1, '1', R),
    (1, E):(2, E, N),
})

print(m)

m.run('01001')

print(m.tape)