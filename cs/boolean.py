# https://docs.sympy.org/latest/modules/logic.html

def create_truth_table(expr):
	
	lenE = len(expr.atoms())
	bvars = sorted(expr.atoms(), key=lambda x: x.name)

	atoms = " ".join([x.name for x in bvars])

	print(f"{atoms} {expr}")


	for i in range(2**lenE):
		# derive imput combination from binary form

		subE = {}
		for j, v in enumerate(reversed(bvars)):
			varV = bool(i & 2**j)
			subE[v] = varV
			# print(j, v, varV)

		subV = int(bool(expr.subs(subE)))
		subs = " ".join(reversed([str(int(v)) for v in subE.values()]))
		print(f"{subs} {subV}")		


