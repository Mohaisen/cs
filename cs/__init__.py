



import os
import sys
import pathlib

__version__ = '0.1.0'

path = pathlib.Path(__file__).parent

def cli():

    scripts = path.parent / 'scripts'

    if len(sys.argv) < 2:

        cmds = [x.stem for x in scripts.iterdir()]

        print(' '.join(cmds))
        return

    cmd = sys.argv[1] 

    cmd_path = scripts / (cmd + '.py')

    if not cmd_path.exists():
        print('No script named %s' % cmd)
        return

    os.system(f'python {cmd_path}')

