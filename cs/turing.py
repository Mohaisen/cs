
class EmptyType():
    def __str__(self): return '⊔'  # '□'
class LeftType():
    def __str__(self): return '←'
    def __int__(self): return -1
class RightType():
    def __str__(self): return '→'
    def __int__(self): return 1
class NeutralType():
    def __str__(self): return '-'  # '↓'
    def __int__(self): return 0
class StartType():
    def __str__(self): return '⊳'

    

Empty = E = EmptyType()
Left = L = LeftType()
Right = R = RightType()
Neutral = N = NeutralType()
Start = S = StartType()


DEFAULT_TRANSITION_LIMIT = 10**2


class Machine():

    def __init__(self, transition, final_states=None, initial_state=None, use_start=True):
        """

        `transition` contains the states implicitly

        `initial_state` defaults to the first one
        `final_states` defaults to the last state
        """
        self._transition = transition
        self.alphabet = set()
        self.states = set()
        self.use_start = use_start

        for k, v in self._transition.items():
            q, a = k
            p, b, d = v
            if d not in (L, N, R):
                raise ValueError('not a valid direction')
            self.states.update([q, p])
            self.alphabet.update([a, b])  # to allow special chars

        # remove non-input
        self.alphabet = {_ for _ in self.alphabet if _ not in (S, E)}

        s = list(self.states)
        self.initial_state = s[0] if initial_state is None else initial_state
        self.final_states = set(s[-1:]) if final_states is None else final_states


    def run(self, tape, debug=False, limit=DEFAULT_TRANSITION_LIMIT):

        print(f'running for input: {tape}')

        self._tape = list(tape)

        if self.use_start:
            self._tape = [Start] + self._tape

        # initialize
        state = self.initial_state
        head_pos = 0  # initial head position

        for i in range(limit):
            
            # read from tape
            if 0 <= head_pos < len(self._tape):
                char = self._tape[head_pos]
            else:
                char = Empty
            
            # try transition
            try:
                p, b, d = self._transition[state, char]
            except KeyError:
                print(f'reject - delta not defined for {state}, {c}')
                return
            
            # change state
            state = p
            
            # write
            if 0 <= head_pos < len(self._tape):
                self._tape[head_pos] = b
            elif head_pos < 0:
                self._tape = [b] + self._tape
                head_pos += 1  # indices shifted
            else:  # pos > len
                self._tape += [b]

            # move
            head_pos += int(d)

            # print steps
            if debug:
                print('step {i}')

            # accept
            if state in self.final_states:
                print(f'accept - state {state}')
                return

        print(f'reject - limit of {limit} transitions reached ~ infinite loop')
        return


    @property
    def tape(self):
        return ''.join([str(_) for _ in self._tape])

    @property
    def transition(self):
        return '\n'.join([
            f"{k[0]}, {k[1]} -> {v[0]}, {v[1]}, {v[2]}"
            for k, v in self._transition.items()
        ])
    

    def __str__(self):
        import textwrap
        return textwrap.dedent(f"""\
        Machine:
            Q = {self.states}
            q₀ = {self.initial_state}
            Ꝗ = {self.final_states}
            Σ = {self.alphabet}
            Γ = Σ ∪ {{'{S}', '{E}'}}
            δ =
        """) + textwrap.indent(self.transition, ' '*8)
