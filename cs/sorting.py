"""
Python program for implementation of
sorintg algorithms
"""
import random
import timeit

SAMPLE_SIZE = 5000
NUMBER_OF_RUNS = 20

def main():

    def test_bubble():
        arr = [random.randrange(1000) for _ in range(SAMPLE_SIZE)]
        bubble_sort(arr)

    def test_merge():
        arr = [random.randrange(1000) for _ in range(SAMPLE_SIZE)]
        merge_sort(arr)

    print(f"Bubble with {SAMPLE_SIZE} samples in {NUMBER_OF_RUNS} runs:")
    print(timeit.timeit(test_bubble, number=NUMBER_OF_RUNS))
    
    print(f"Merge with {SAMPLE_SIZE} samples in {NUMBER_OF_RUNS} runs:")
    print(timeit.timeit(test_merge, number=NUMBER_OF_RUNS))


def bubble_sort(arr):
    n = len(arr)

    # Traverse through all array elements
    for i in range(n):

        # traverse the array from 0 to n-i-1
        #   last i elements are already in place
        for j in range(0, n-i-1):

            # Swap if the element found is greater than the next element
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]


def merge_sort(arr): 
    if len(arr) >1: 
        mid = len(arr)//2 #Finding the mid of the array 
        A = arr[:mid] # Dividing the array elements  
        B = arr[mid:] # into 2 halves 
  
        merge_sort(A) # Sorting the first half 
        merge_sort(B) # Sorting the second half 
  
        i = j = k = 0
          
        # Copy data from temp arrays A[] and B[] 
        while i < len(A) and j < len(B): 
            if A[i] < B[j]: 
                arr[k] = A[i] 
                i+=1
            else: 
                arr[k] = B[j] 
                j+=1
            k+=1
          
        # Checking if any element was left 
        while i < len(A): 
            arr[k] = A[i] 
            i+=1
            k+=1
          
        while j < len(B): 
            arr[k] = B[j] 
            j+=1
            k+=1
  
  
if __name__ == '__main__': main()

